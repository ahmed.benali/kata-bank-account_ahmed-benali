using kata_bank_account.Extensions;
using kata_bank_account.Helpers;
using System;
using System.Linq;
using Xunit;

namespace kata_bank_account_tests
{
    public class CustomerTests
    {
        [Fact]
        public void InitiateCustomer()
        {
            var customer = CustomerHelper.GetDummyCustomerWithEmptyAccount("1");
            Assert.Equal("Aragorn", customer.Name);
            Assert.Single(customer.Accounts);
            Assert.Equal(0, customer.Accounts.FirstOrDefault().Credit);
        }

        [Fact]
        public void AccountOperationsTests()
        {
            var customer = CustomerHelper.GetDummyCustomerWithEmptyAccount("1");
            customer.Accounts.FirstOrDefault().DoDeposit(100);
            Assert.Equal(100, customer.Accounts.FirstOrDefault().Credit);
            Assert.Single(customer.Accounts.FirstOrDefault().Transactions);
            customer.Accounts.FirstOrDefault().DoRetreat(101);
            Assert.Single(customer.Accounts.FirstOrDefault().Transactions);
            customer.Accounts.FirstOrDefault().DoRetreat(90);
            Assert.Equal(2, customer.Accounts.FirstOrDefault().Transactions.Count);
        }
    }
}

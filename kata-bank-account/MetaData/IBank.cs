﻿using kata_bank_account.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.MetaData
{
    interface IBank
    {
        string Id { get; set; }
        string Name { get; set; }
        List<Customer> Customers { get; set; }
    }
}

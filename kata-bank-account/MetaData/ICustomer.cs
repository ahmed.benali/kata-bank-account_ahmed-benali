﻿using kata_bank_account.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.MetaData
{
    interface ICustomer
    {
        string Id { get; set; }
        string Name { get; set; }
        string BankId { get; set; }
        List<Account> Accounts { get; set; }
    }
}

﻿using kata_bank_account.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.MetaData
{
    interface ITransaction
    {
        string Number { get; set; }
        TransactionType TransactionType { get; set; }
        decimal Value { get; set; }
        DateTime Date { get; set; }
    }
}

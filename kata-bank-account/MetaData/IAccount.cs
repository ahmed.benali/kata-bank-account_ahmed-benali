﻿using kata_bank_account.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.MetaData
{
    interface IAccount
    {
        string Id { get; set; }
        string CustomerId { get; set; }
        string BankId { get; set; }
        decimal Credit { get; set; }
        List<Transaction> Transactions { get; set; }
    }
}

﻿using kata_bank_account.Helpers;
using kata_bank_account.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.Extensions
{
    public static class AccountExtensions
    {
        public static void DoDeposit(this Account account, decimal value)
        {
            var newTransaction = new Transaction() { Number = (account.Transactions.Count + 1).ToString(), Date = DateTime.Now, TransactionType = TransactionType.Deposit, Value = value };
            account.Transactions.Add(newTransaction);
            account.Credit += value;
            Console.WriteLine($"You have deposit successfully an amount of : {value} EUR");
        }
        public static void Deposit(this Account account)
        {
            string UserInput;
            ConsoleChoiceHelper.PrintChoices(null, "Please enter the amount to deposit", out UserInput);
            decimal value;
            while (!Decimal.TryParse(UserInput, out value))
            {
                ConsoleChoiceHelper.PrintChoices(null, "Please enter the amount to deposit", out UserInput);
            }
            account.DoDeposit(value);
        }

        public static void DoRetreat(this Account account, decimal value)
        {
            if (account.Credit >= value)
            {
                var newTransaction = new Transaction() { Number = (account.Transactions.Count + 1).ToString(), Date = DateTime.Now, TransactionType = TransactionType.Retreat, Value = value };
                account.Transactions.Add(newTransaction);
                account.Credit -= value;
                Console.WriteLine($"You have retrated successfully an amount of : {value} EUR");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Your balance is insufficient to make this transaction");
                Console.ResetColor();
            }
        }
        public static void Retreat(this Account account)
        {
            string UserInput;
            ConsoleChoiceHelper.PrintChoices(null, "Please enter the amount to retreat", out UserInput);
            decimal value;
            while (!Decimal.TryParse(UserInput, out value))
            {
                ConsoleChoiceHelper.PrintChoices(null, "Please enter the amount to retreat", out UserInput);
            }
            account.DoRetreat(value);
        }

        public static void ShowHistory(this Account account)
        {
            Console.WriteLine($"Your balance is : {account.Credit}");
            if (account.Transactions is null || account.Transactions.Count == 0)
            {
                Console.WriteLine("You have not made any transactions yet");
            }
            else
            {
                foreach (var transaction in account.Transactions)
                {
                    if (transaction.TransactionType == TransactionType.Deposit)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"  + {transaction.Value} EUR - At {transaction.Date.ToString("dddd, dd MMMM yyyy HH:mm:ss")}");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"  - {transaction.Value} EUR - At {transaction.Date.ToString("dddd, dd MMMM yyyy HH:mm:ss")}");
                    }
                }
                Console.ResetColor();
            }
        }
    }
}

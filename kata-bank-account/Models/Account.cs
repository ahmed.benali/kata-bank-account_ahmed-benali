﻿using kata_bank_account.Helpers;
using kata_bank_account.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.Models
{
    public class Account : IAccount
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string BankId { get; set; }
        public decimal Credit { get; set; }
        public List<Transaction> Transactions { get; set; }
        
    }
}

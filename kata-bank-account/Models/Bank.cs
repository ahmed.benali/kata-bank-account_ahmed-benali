﻿using kata_bank_account.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.Models
{
    public class Bank : IBank
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<Customer> Customers { get; set; }
    }
}

﻿using kata_bank_account.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.Models
{
    public class Customer : ICustomer
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string BankId { get; set; }
        public List<Account> Accounts { get; set; }
    }
}

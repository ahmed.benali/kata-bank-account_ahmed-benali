﻿using kata_bank_account.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.Models
{
    public class Transaction : ITransaction
    {
        public string Number { get; set; }
        public TransactionType TransactionType { get; set; }
        public decimal Value { get; set; }
        public DateTime Date { get; set; }
    }
    public enum TransactionType
    {
        Deposit,
        Retreat
    }
}

﻿using kata_bank_account.Extensions;
using kata_bank_account.Helpers;
using kata_bank_account.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kata_bank_account
{
    class Program
    {
        static void Main(string[] args)
        {
            string UserInput;
            List<Choice> BankChoices = new List<Choice> {
                new Choice(){ChoiceNumber = "1", ChoiceText = "Men"},
                new Choice(){ChoiceNumber = "2", ChoiceText = "Elves"},
                new Choice(){ChoiceNumber = "3", ChoiceText = "Hobbits"},
                new Choice(){ChoiceNumber = "4", ChoiceText = "Dwarves"},
                new Choice(){ChoiceNumber = "5", ChoiceText = "Wizards"},
            };
            ConsoleChoiceHelper.PrintChoices(BankChoices, "Welcome to Middle Earth Banks \n Please chose your bank", out UserInput);
            var customer = CustomerHelper.GetDummyCustomerWithEmptyAccount(UserInput);
            ConsoleChoiceHelper.PrintChoices(null, "Please enter your account number", out UserInput);
            ConsoleChoiceHelper.PrintChoices(null, "Please enter your password", out UserInput);

            List<Choice> Choices = new List<Choice> {
                new Choice(){ChoiceNumber = "1", ChoiceText = "Deposit"},
                new Choice(){ChoiceNumber = "2", ChoiceText = "Retreat"},
                new Choice(){ChoiceNumber = "3", ChoiceText = "Show history"},
                new Choice(){ChoiceNumber = "4", ChoiceText = "Disconnect"},
            };
            string welcome = $"--------------------- \n Welcome {customer.Name} \n ---------------------";
            ConsoleChoiceHelper.PrintChoices(Choices, welcome, out UserInput);

            while(UserInput != "4")
            {
                switch (UserInput)
                {
                    case "1":
                        customer.Accounts.FirstOrDefault().Deposit();
                        break;
                    case "2":
                        customer.Accounts.FirstOrDefault().Retreat();
                        break;
                    case "3":
                        customer.Accounts.FirstOrDefault().ShowHistory();
                        break;
                }
                ConsoleChoiceHelper.PrintChoices(Choices, welcome, out UserInput);
            }
            Console.WriteLine("Thank you for using Middle Earth Bank services");
        }
    }
}

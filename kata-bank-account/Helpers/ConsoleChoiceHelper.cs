﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kata_bank_account.Helpers
{
    public static class ConsoleChoiceHelper
    {
        public static void PrintChoices(List<Choice> Choices, string Message, out string UserInput)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Message + ":");
            Console.ResetColor();
            if (Choices != null && Choices.Count > 0)
            {
                foreach (var choice in Choices)
                {
                    Console.WriteLine($"* {choice.ChoiceText} : {choice.ChoiceNumber}");
                }
                UserInput = Console.ReadLine();
                while (!Choices.Select(c => c.ChoiceNumber).Contains(UserInput))
                {
                    Console.WriteLine("Invalid choice, please type choice again");
                    UserInput = Console.ReadLine();
                }
            }
            else
            {
                UserInput = Console.ReadLine();
                while (String.IsNullOrWhiteSpace(UserInput))
                {
                    Console.WriteLine(Message);
                    UserInput = Console.ReadLine();
                }
            }
        }
    }

    public class Choice
    {
        public string ChoiceNumber { get; set; }
        public string ChoiceText { get; set; }
    }
}

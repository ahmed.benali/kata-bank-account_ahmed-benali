﻿using kata_bank_account.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace kata_bank_account.Helpers
{
    public static class CustomerHelper
    {
        public static Customer GetDummyCustomerWithEmptyAccount(string BankId)
        {
            Dictionary<string, string> BankCustomer = new Dictionary<string, string>()
            {
                { "1","Aragorn"},
                { "2","Galadriel"},
                { "3","Frodo"},
                { "4","Gimli"},
                { "5","Sauron"},
            };
            Account emptyAccount = new Account() { Id = "1", BankId = BankId, Credit = 0, CustomerId = "1", Transactions = new List<Transaction>() };
            List<Account> accounts = new List<Account>() { emptyAccount };
            Customer customer = new Customer() { Id = "1", BankId = BankId, Name = BankCustomer[BankId], Accounts = accounts };
            return customer;
        }
    }
}
